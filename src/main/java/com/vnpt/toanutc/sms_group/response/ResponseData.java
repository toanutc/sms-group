package com.vnpt.toanutc.sms_group.response;

import com.vnpt.toanutc.sms_group.constants.ResponseConstants;
import lombok.Data;

@Data
public class ResponseData {
	private Object data;
	private String message;
	private int statusCode;
	private String version = "1.0.2";

	public ResponseData() {
		this.setStatusCode(ResponseConstants.StatusCode.OK);
		this.setMessage(ResponseConstants.Message.SUCCESS);
	}

}
