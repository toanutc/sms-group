package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.SendSmsHistory;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.SendSmsHistoryRepository;
import com.vnpt.toanutc.sms_group.services.SendSmsHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendSmsHistoryServiceImpl extends BaseServiceImpl<SendSmsHistory, Integer, SendSmsHistory>
        implements SendSmsHistoryService {

    @Autowired
    private SendSmsHistoryRepository repository;

    @Override
    protected BaseRepository<SendSmsHistory, Integer, SendSmsHistory> getRepository() {
        return repository;
    }

}