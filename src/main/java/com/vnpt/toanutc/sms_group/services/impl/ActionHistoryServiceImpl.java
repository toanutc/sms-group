package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.ActionHistory;
import com.vnpt.toanutc.sms_group.repositories.ActionHistoryRepository;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.services.ActionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActionHistoryServiceImpl extends BaseServiceImpl<ActionHistory, Integer, ActionHistory>
        implements ActionHistoryService {

    @Autowired
    private ActionHistoryRepository repository;

    @Override
    protected BaseRepository<ActionHistory, Integer, ActionHistory> getRepository() {
        return repository;
    }

}