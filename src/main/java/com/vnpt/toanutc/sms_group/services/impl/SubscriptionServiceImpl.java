package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.Subscription;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.SubscriptionRepository;
import com.vnpt.toanutc.sms_group.services.SubscriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscriptionServiceImpl extends BaseServiceImpl<Subscription, Integer, Subscription>
        implements SubscriptionService {

    @Autowired
    private SubscriptionRepository repository;

    @Override
    protected BaseRepository<Subscription, Integer, Subscription> getRepository() {
        return repository;
    }

    @Override
    public List<Subscription> removeAllByPhoneNumber(String phoneNumber) {
        return repository.removeAllByPhoneNumber(phoneNumber);
    }

    @Override
    public Subscription getSubscriptionByPhoneNumberAndStatus(String phoneNumber, int status) {
        return repository.getSubscriptionByPhoneNumberAndStatus(phoneNumber, status);
    }

}