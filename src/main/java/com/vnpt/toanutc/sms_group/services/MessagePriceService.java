package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.MessagePrice;

public interface MessagePriceService extends BaseService<MessagePrice, Integer, MessagePrice> {
    MessagePrice getMessagePriceByType(int type);
}