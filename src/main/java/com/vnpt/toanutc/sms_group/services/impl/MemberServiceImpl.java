package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.Member;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.MemberRepository;
import com.vnpt.toanutc.sms_group.services.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberServiceImpl extends BaseServiceImpl<Member, Integer, Member>
        implements MemberService {

    @Autowired
    private MemberRepository repository;

    @Override
    protected BaseRepository<Member, Integer, Member> getRepository() {
        return repository;
    }

    @Override
    public List<Member> getMembersByGroupId(int groupId) {
        return repository.getMembersByGroupId(groupId);
    }

    @Override
    public List<Member> removeAllByGroupId(int groupId) {
        return repository.removeAllByGroupId(groupId);
    }

    @Override
    public List<Member> removeAllByGroupIdAndAndMemberNumber(int groupId, String memberNumber) {
        return repository.removeAllByGroupIdAndAndMemberNumber(groupId, memberNumber);
    }
}