package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.authentication.UserAuthenticationDetail;
import com.vnpt.toanutc.sms_group.models.domains.User;
import com.vnpt.toanutc.sms_group.models.domains.UserRole;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.UserRepository;
import com.vnpt.toanutc.sms_group.repositories.UserRoleRepository;
import com.vnpt.toanutc.sms_group.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends BaseServiceImpl<User, Integer, User>
        implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    protected BaseRepository<User, Integer, User> getRepository() {
        return repository;
    }

    @Override
    public User findByUserName(String userName) {
        return repository.findByUserName(userName);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException(userName);
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        List<UserRole> userRoles = userRoleRepository.getUserRolesByUserName(userName);
        for (UserRole userRole : userRoles) {
            grantedAuthorities.add(new SimpleGrantedAuthority(userRole.getRoleCode()));
        }

        return new UserAuthenticationDetail(user, grantedAuthorities);
    }
}