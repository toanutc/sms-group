package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.SmsGroup;

import java.util.List;

public interface SmsGroupService extends BaseService<SmsGroup, Integer, SmsGroup> {
    SmsGroup getSmsGroupByGroupNameAndSubId(String groupName, int subId);

    List<SmsGroup> removeAllBySubId(int subId);
}