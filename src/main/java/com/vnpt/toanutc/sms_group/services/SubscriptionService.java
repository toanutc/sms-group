package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.Subscription;

import java.util.List;

public interface SubscriptionService extends BaseService<Subscription, Integer, Subscription> {
    List<Subscription> removeAllByPhoneNumber(String phoneNumber);

    Subscription getSubscriptionByPhoneNumberAndStatus(String phoneNumber, int status);
}