package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.Member;

import java.util.List;

public interface MemberService extends BaseService<Member, Integer, Member> {
    List<Member> getMembersByGroupId(int groupId);

    List<Member> removeAllByGroupId(int groupId);

    List<Member> removeAllByGroupIdAndAndMemberNumber(int groupId, String memberNumber);
}