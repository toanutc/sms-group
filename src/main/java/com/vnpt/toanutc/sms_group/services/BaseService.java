package com.vnpt.toanutc.sms_group.services;

import java.util.List;
import java.util.Optional;

public interface BaseService<E, K, DTO> {

    <S extends E> S save(S entity);

    <S extends E> List<S> saveAll(List<S> entities);

    Optional<E> findById(K key);

    boolean existsById(K key);

    List<E> findAll();

    List<E> findAllById(List<K> keys);

    long count();

    void deleteById(K key);

    void delete(E entity);

    void deleteAll(List<? extends E> entities);

    void deleteAll();

}