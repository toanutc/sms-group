package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.ActionHistory;

public interface ActionHistoryService extends BaseService<ActionHistory, Integer, ActionHistory> {

}