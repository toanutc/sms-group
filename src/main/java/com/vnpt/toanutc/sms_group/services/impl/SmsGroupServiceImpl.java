package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.SmsGroup;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.SmsGroupRepository;
import com.vnpt.toanutc.sms_group.services.SmsGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmsGroupServiceImpl extends BaseServiceImpl<SmsGroup, Integer, SmsGroup>
        implements SmsGroupService {

    @Autowired
    private SmsGroupRepository repository;

    @Override
    protected BaseRepository<SmsGroup, Integer, SmsGroup> getRepository() {
        return repository;
    }

    @Override
    public SmsGroup getSmsGroupByGroupNameAndSubId(String groupName, int subId) {
        return repository.getSmsGroupByGroupNameAndSubId(groupName, subId);
    }

    @Override
    public List<SmsGroup> removeAllBySubId(int subId) {
        return repository.removeAllBySubId(subId);
    }
}