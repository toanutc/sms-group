package com.vnpt.toanutc.sms_group.services.impl;

import com.vnpt.toanutc.sms_group.models.domains.MessagePrice;
import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.repositories.MessagePriceRepository;
import com.vnpt.toanutc.sms_group.services.MessagePriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessagePriceServiceImpl extends BaseServiceImpl<MessagePrice, Integer, MessagePrice>
        implements MessagePriceService {

    @Autowired
    private MessagePriceRepository repository;

    @Override
    protected BaseRepository<MessagePrice, Integer, MessagePrice> getRepository() {
        return repository;
    }

    @Override
    public MessagePrice getMessagePriceByType(int type) {
        return repository.getMessagePriceByType(type);
    }
}