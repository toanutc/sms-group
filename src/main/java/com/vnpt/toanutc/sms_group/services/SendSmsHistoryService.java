package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.SendSmsHistory;

public interface SendSmsHistoryService extends BaseService<SendSmsHistory, Integer, SendSmsHistory> {

}