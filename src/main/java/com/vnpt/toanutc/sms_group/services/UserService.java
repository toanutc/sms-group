package com.vnpt.toanutc.sms_group.services;

import com.vnpt.toanutc.sms_group.models.domains.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends BaseService<User, Integer, User>, UserDetailsService {
    User findByUserName(String userName);
}