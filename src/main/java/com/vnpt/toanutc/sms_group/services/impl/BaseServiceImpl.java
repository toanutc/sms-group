package com.vnpt.toanutc.sms_group.services.impl;

import java.util.List;
import java.util.Optional;

import com.vnpt.toanutc.sms_group.repositories.BaseRepository;
import com.vnpt.toanutc.sms_group.services.BaseService;

public abstract class BaseServiceImpl<E, K, DTO> implements BaseService<E, K, DTO> {

    protected abstract BaseRepository<E, K, DTO> getRepository();

    @Override
    public <S extends E> S save(S entity) {
        return getRepository().saveAndFlush(entity);
    }

    @Override
    public <S extends E> List<S> saveAll(List<S> entities) {
        return getRepository().saveAll(entities);
    }

    @Override
    public Optional<E> findById(K key) {
        return getRepository().findById(key);
    }

    @Override
    public boolean existsById(K key) {
        return getRepository().existsById(key);
    }

    @Override
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @Override
    public List<E> findAllById(List<K> keys) {
        return getRepository().findAllById(keys);
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public void deleteById(K key) {
        getRepository().deleteById(key);
    }

    @Override
    public void delete(E entity) {
        getRepository().delete(entity);
    }

    @Override
    public void deleteAll(List<? extends E> entities) {
        getRepository().deleteAll(entities);
    }

    @Override
    public void deleteAll() {
        getRepository().deleteAll();
    }

}