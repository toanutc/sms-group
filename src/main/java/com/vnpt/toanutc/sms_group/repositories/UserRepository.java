package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User, Integer, User> {
    User findByUserName(String userName);
}