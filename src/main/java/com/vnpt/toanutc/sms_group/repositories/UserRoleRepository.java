package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.UserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends BaseRepository<UserRole, Integer, UserRole> {

    List<UserRole> getUserRolesByUserName(String userName);
}