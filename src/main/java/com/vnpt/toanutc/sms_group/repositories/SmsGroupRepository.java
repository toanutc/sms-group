package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.SmsGroup;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmsGroupRepository extends BaseRepository<SmsGroup, Integer, SmsGroup> {
    SmsGroup getSmsGroupByGroupNameAndSubId(String groupName, int subId);

    List<SmsGroup> removeAllBySubId(int subId);
}