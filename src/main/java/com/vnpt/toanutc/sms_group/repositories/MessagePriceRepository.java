package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.MessagePrice;
import org.springframework.stereotype.Repository;

@Repository
public interface MessagePriceRepository extends BaseRepository<MessagePrice, Integer, MessagePrice> {
    MessagePrice getMessagePriceByType(int type);
}