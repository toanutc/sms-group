package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.Member;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberRepository extends BaseRepository<Member, Integer, Member> {
    List<Member> getMembersByGroupId(int groupId);

    List<Member> removeAllByGroupId(int groupId);

    List<Member> removeAllByGroupIdAndAndMemberNumber(int groupId, String memberNumber);
}