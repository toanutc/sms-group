package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.ActionHistory;
import com.vnpt.toanutc.sms_group.models.domains.SmsGroup;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionHistoryRepository extends BaseRepository<ActionHistory, Integer, ActionHistory> {
    
}