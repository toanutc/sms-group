package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.SendSmsHistory;
import org.springframework.stereotype.Repository;

@Repository
public interface SendSmsHistoryRepository extends BaseRepository<SendSmsHistory, Integer, SendSmsHistory> {

}