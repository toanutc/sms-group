package com.vnpt.toanutc.sms_group.repositories;

import com.vnpt.toanutc.sms_group.models.domains.Subscription;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends BaseRepository<Subscription, Integer, Subscription> {

    List<Subscription> removeAllByPhoneNumber(String phoneNumber);

    Subscription getSubscriptionByPhoneNumberAndStatus(String phoneNumber, int status);

    boolean existsAllByPhoneNumberAndStatus(String phoneNumber, int status);
}