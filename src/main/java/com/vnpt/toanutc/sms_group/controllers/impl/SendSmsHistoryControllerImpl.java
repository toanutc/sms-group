package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.models.domains.SendSmsHistory;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.SendSmsHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/send-sms-history")
public class SendSmsHistoryControllerImpl extends BaseControllerImpl<SendSmsHistory, Integer, SendSmsHistory> {

    @Autowired
    private SendSmsHistoryService service;

    @Override
    public BaseService<SendSmsHistory, Integer, SendSmsHistory> getService() {
        return service;
    }

}