package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.models.domains.SmsGroup;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.SmsGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/sms-group")
public class SmsGroupControllerImpl extends BaseControllerImpl<SmsGroup, Integer, SmsGroup> {

    @Autowired
    private SmsGroupService service;

    @Override
    public BaseService<SmsGroup, Integer, SmsGroup> getService() {
        return service;
    }

}