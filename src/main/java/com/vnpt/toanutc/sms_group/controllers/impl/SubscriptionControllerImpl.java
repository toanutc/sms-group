package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.models.domains.Subscription;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/subscription")
public class SubscriptionControllerImpl extends BaseControllerImpl<Subscription, Integer, Subscription> {

    @Autowired
    private SubscriptionService service;

    @Override
    public BaseService<Subscription, Integer, Subscription> getService() {
        return service;
    }
}