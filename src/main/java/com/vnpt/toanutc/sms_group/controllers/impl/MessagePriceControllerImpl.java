package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.models.domains.MessagePrice;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.MessagePriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/message-price")
public class MessagePriceControllerImpl extends BaseControllerImpl<MessagePrice, Integer, MessagePrice> {

    @Autowired
    private MessagePriceService service;

    @Override
    public BaseService<MessagePrice, Integer, MessagePrice> getService() {
        return service;
    }

}