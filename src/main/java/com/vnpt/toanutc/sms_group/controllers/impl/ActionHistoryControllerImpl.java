package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.constants.ActionCodeConstants;
import com.vnpt.toanutc.sms_group.constants.CommonConstants;
import com.vnpt.toanutc.sms_group.models.domains.*;
import com.vnpt.toanutc.sms_group.services.*;
import com.vnpt.toanutc.sms_group.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/action")
@Slf4j
public class ActionHistoryControllerImpl extends BaseControllerImpl<ActionHistory, Integer, ActionHistory> {

    @Autowired
    private ActionHistoryService service;

    @Autowired
    private SmsGroupService smsGroupService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private MessagePriceService messagePriceService;

    @Autowired
    private SendSmsHistoryService sendSmsHistoryService;


    @Override
    public BaseService<ActionHistory, Integer, ActionHistory> getService() {
        return service;
    }

    @Override
    public ResponseEntity<?> add(ActionHistory entity) {
        ResponseEntity responseEntity = super.add(entity);

        if (entity.getActionCode() == ActionCodeConstants.CREATE) {
            registerService(entity);
        }

        Subscription subscription = subscriptionService.getSubscriptionByPhoneNumberAndStatus(entity.getPhoneNumber(), CommonConstants.Status.ACTIVE);
        if (subscription == null)
            return responseEntity;

        switch (entity.getActionCode()) {
            case ActionCodeConstants.CANCEL:
                cancelService(entity, subscription);
                break;
            case ActionCodeConstants.CREATE:
                createSmsGroup(entity, subscription);
                break;
            case ActionCodeConstants.ADD:
                addMember(entity, subscription);
                break;
            case ActionCodeConstants.SEND:
                sendSms(entity, subscription);
                break;
            case ActionCodeConstants.DELETE:
                deleteSmsGroupOrUpdateMember(entity, subscription);
                break;
            default:
                log.error("Don't have a action accepted");
                break;
        }
        return responseEntity;
    }


    private boolean registerService(ActionHistory entity) {
        Subscription subscription = Subscription.builder()
                .phoneNumber(entity.getPhoneNumber())
                .channel(entity.getChannel())
                .status(CommonConstants.Status.ACTIVE)
                .createdDate(entity.getCreatedDate())
                .build();

        return subscriptionService.save(subscription) != null;
    }

    private boolean cancelService(ActionHistory entity, Subscription subscription) {
        try {
            subscriptionService.removeAllByPhoneNumber(subscription.getPhoneNumber());
            List<SmsGroup> groupList = smsGroupService.removeAllBySubId(subscription.getID());
            for (SmsGroup smsGroup : groupList) {
                memberService.removeAllByGroupId(smsGroup.getID());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    private boolean createSmsGroup(ActionHistory entity, Subscription subscription) {

        if (StringUtils.isNullOrEmpty(entity.getParams()))
            return false;
        try {
            String[] valueParams = entity.getParams().split(" ");
            SmsGroup result = addSmsGroup(subscription.getID(), valueParams, entity);
            addMember(result.getID(), valueParams, entity);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    private SmsGroup addSmsGroup(int idSub, String[] valueParams, ActionHistory entity) {
        String smsGroupName = valueParams[0];

        SmsGroup smsGroup = SmsGroup.builder()
                .groupName(smsGroupName)
                .channel(entity.getChannel())
                .createdDate(entity.getCreatedDate())
                .subId(idSub)
                .build();
        return smsGroupService.save(smsGroup);
    }

    private void addMember(int idSmsGroup, String[] valueParams, ActionHistory entity) {
        try {
            for (int i = 1; i < valueParams.length; i++) {
                Member member = Member.builder()
                        .memberNumber(valueParams[i])
                        .createdDate(entity.getCreatedDate())
                        .groupId(idSmsGroup)
                        .build();
                memberService.save(member);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private boolean addMember(ActionHistory entity, Subscription subscription) {
        if (StringUtils.isNullOrEmpty(entity.getParams()))
            return false;
        try {
            String[] valueParams = entity.getParams().split(" ");
            SmsGroup result = smsGroupService.getSmsGroupByGroupNameAndSubId(valueParams[0], subscription.getID());
            addMember(result.getID(), valueParams, entity);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    private boolean sendSms(ActionHistory entity, Subscription subscription) {
        if (StringUtils.isNullOrEmpty(entity.getParams())) {
            return false;
        }
        try {
            String[] valueParams = entity.getParams().split(" ");
            String messageContent = valueParams[1];

            SmsGroup result = smsGroupService.getSmsGroupByGroupNameAndSubId(valueParams[0], subscription.getID());
            List<Member> memberList = memberService.getMembersByGroupId(result.getID());

            for (int i = 0; i < memberList.size(); i++) {
                Member member = memberList.get(i);
                int priceMessage = getMessagePrice(subscription.getPhoneNumber(), member);
                SendSmsHistory sendSmsHistory = SendSmsHistory.builder()
                        .message(messageContent)
                        .createdDate(entity.getCreatedDate())
                        .fromPhoneNumber(subscription.getPhoneNumber())
                        .toPhoneNumber(member.getMemberNumber())
                        .sentTime(entity.getCreatedDate())
                        .status(CommonConstants.StatusSend.SENDING)
                        .messagePrice(priceMessage)
                        .build();

                sendSmsHistoryService.save(sendSmsHistory);

                //sendSmsTôModule();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

    private int getMessagePrice(String phoneNumber, Member member) {
        MessagePrice messagePrice = messagePriceService.findById(member.getPriceId()).get();
        int priceOther = 0;
        if (checkSameNetwork(phoneNumber, member.getMemberNumber())) {
            priceOther = messagePriceService.getMessagePriceByType(2).getMessagePrice();
        }
        return messagePrice.getMessagePrice() + priceOther;
    }

    private boolean checkSameNetwork(String phoneNumber, String memberNumber) {
        return false;
    }


    private boolean deleteSmsGroupOrUpdateMember(ActionHistory entity, Subscription subscription) {
        if (StringUtils.isNullOrEmpty(entity.getParams())) {
            return false;
        }

        try {
            String[] valueParams = entity.getParams().split(" ");
            SmsGroup result = smsGroupService.getSmsGroupByGroupNameAndSubId(valueParams[0], subscription.getID());

            if (valueParams.length > 1) {
                for (int i = 0; i < valueParams.length; i++) {
                    memberService.removeAllByGroupIdAndAndMemberNumber(result.getID(), valueParams[i]);
                }
            } else {
                memberService.removeAllByGroupId(result.getID());
                smsGroupService.deleteById(result.getID());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
        return true;
    }

}