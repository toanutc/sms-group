package com.vnpt.toanutc.sms_group.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface BaseController<E, K, DTO> {

    ResponseEntity<?> getAll();

    ResponseEntity<?> getById(K key);

    ResponseEntity<?> add(E entity);

    ResponseEntity<?> addAll(List<E> values);

    ResponseEntity<?> update(E value);

    ResponseEntity<?> delete(E value);

    ResponseEntity<?> deleteById(K key);

    ResponseEntity<?> deleteAll(List<E> values);

}