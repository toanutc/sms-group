package com.vnpt.toanutc.sms_group.controllers.impl;

import java.util.List;
import java.util.Optional;

import com.vnpt.toanutc.sms_group.constants.ActionCodeConstants;
import com.vnpt.toanutc.sms_group.controllers.BaseController;
import com.vnpt.toanutc.sms_group.response.ResponseData;
import com.vnpt.toanutc.sms_group.services.BaseService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class BaseControllerImpl<E, K, DTO> implements BaseController<E, K, DTO> {

    protected ResponseData result;

    public abstract BaseService<E, K, DTO> getService();

    @GetMapping("/all")
    @Override
    public ResponseEntity<?> getAll() {
        result = new ResponseData();
        List<E> data = getService().findAll();
        result.setData(data);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<?> getById(@PathVariable("id") K key) {
        result = new ResponseData();
        Optional<E> data = getService().findById(key);
        result.setData(data);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/add")
    @Override
    public ResponseEntity<?> add(@RequestBody E entity) {
        result = new ResponseData();
        E data = getService().save(entity);
        result.setData(data);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/add-all")
    @Override
    public ResponseEntity<?> addAll(@RequestBody List<E> values) {
        result = new ResponseData();
        List<E> data = getService().saveAll(values);
        result.setData(data);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/update")
    @Override
    public ResponseEntity<?> update(@RequestBody E value) {
        result = new ResponseData();
        E data = getService().save(value);
        result.setData(data);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/delete")
    @Override
    public ResponseEntity<?> delete(@RequestBody E value) {
        result = new ResponseData();
        getService().delete(value);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/delete/{id}")
    @Override
    public ResponseEntity<?> deleteById(@PathVariable("id") K key) {
        result = new ResponseData();
        getService().deleteById(key);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/delete-all")
    @Override
    public ResponseEntity<?> deleteAll(@RequestBody List<E> values) {
        result = new ResponseData();
        getService().deleteAll(values);
        return ResponseEntity.ok(result);
    }
}