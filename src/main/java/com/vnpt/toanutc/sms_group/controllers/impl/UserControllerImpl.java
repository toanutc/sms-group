package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.authentication.UserAuthenticationDetail;
import com.vnpt.toanutc.sms_group.authentication.JwtTokenProvider;
import com.vnpt.toanutc.sms_group.models.domains.User;
import com.vnpt.toanutc.sms_group.response.ResponseData;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.vnpt.toanutc.sms_group.constants.AuthenticationConstants.HEADER_STRING;
import static com.vnpt.toanutc.sms_group.constants.AuthenticationConstants.TOKEN_PREFIX;

@Controller
@RequestMapping("/user")
public class UserControllerImpl extends BaseControllerImpl<User, Integer, User> {


    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private UserService service;

    @Override
    public BaseService<User, Integer, User> getService() {
        return service;
    }


    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(HttpServletRequest request, HttpServletResponse response, @RequestBody User user) {
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getUserName(),
                            user.getPassword()
                    )
            );
        } catch (Exception e) {
            ResponseEntity.ok(e.getMessage());
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwtToken = tokenProvider.generateToken((UserAuthenticationDetail) authentication.getPrincipal());
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + jwtToken);

        result = new ResponseData();
        User data = service.findByUserName(user.getUserName());
        result.setData(data);

        return ResponseEntity.ok(result);
    }
}