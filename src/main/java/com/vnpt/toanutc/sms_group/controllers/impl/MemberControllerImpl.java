package com.vnpt.toanutc.sms_group.controllers.impl;

import com.vnpt.toanutc.sms_group.models.domains.Member;
import com.vnpt.toanutc.sms_group.services.BaseService;
import com.vnpt.toanutc.sms_group.services.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/member")
public class MemberControllerImpl extends BaseControllerImpl<Member, Integer, Member> {

    @Autowired
    private MemberService service;

    @Override
    public BaseService<Member, Integer, Member> getService() {
        return service;
    }

}