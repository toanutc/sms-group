package com.vnpt.toanutc.sms_group.constants;

public class ResponseConstants {

    public static class StatusCode {
        public static int OK = 200;
        public static int BAD_REQUEST = 500;
    }

    public static class Message {
        public static String SUCCESS = "Lấy dữ liệu thành công";
        public static String ERROR = "Xảy ra lỗi trong quá trình lấy dữ liệu";
    }

}