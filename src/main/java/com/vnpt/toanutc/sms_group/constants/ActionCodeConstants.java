package com.vnpt.toanutc.sms_group.constants;

public class ActionCodeConstants {
    public static final String CREATE = "TAO";
    public static final String ADD = "THEM";
    public static final String DELETE = "XOA";
    public static final String UPDATE = "SUA";
    public static final String SEND = "GUI";
    public static final String REGISTER = "DK";
    public static final String CANCEL = "HUY";
}
