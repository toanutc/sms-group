package com.vnpt.toanutc.sms_group.constants;

public class AuthenticationConstants {
    public static final String SECRET = "ThisIsASecret";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String JWT_SECRET = "toanutc";
    public static final long JWT_EXPIRATION = 604800000L;
}
