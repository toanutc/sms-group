package com.vnpt.toanutc.sms_group.constants;

public class CommonConstants {
    public static class Status {
        public static int ACTIVE = 1;
        public static int INACTIVE = 0;
    }

    public static class StatusSend {
        public static String SENDING = "SENDING";
        public static String SENT = "SENT";
    }
}
