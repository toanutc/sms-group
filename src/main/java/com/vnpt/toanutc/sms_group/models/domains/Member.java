package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name = "members")
@Builder
public class Member implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4599323381079731131L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "GROUP_ID")
    private int groupId;

    @Column(name = "MEMBER_NUMBER")
    private String memberNumber;

    @Column(name = "MEMBER_NAME")
    private String memberName;

    @Column(name = "CREATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "PRICE_ID")
    private int priceId;

}