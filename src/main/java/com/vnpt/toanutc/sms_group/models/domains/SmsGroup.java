package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name = "sms_groups")
@Builder
public class SmsGroup implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3355753859183485439L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "SUB_ID")
    private int subId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "CREATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "CHANNEL")
    private String channel;

}