package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "subscriptions")
@Builder
public class Subscription implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4117942183537420305L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "PHONE_NUMBER", unique = true)
    private String phoneNumber;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CREATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "CHANNEL")
    private String channel;

    @Column(name = "AUTO_RENEW")
    private int autoRenew;

    @Column(name = "EXPIRE_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date expireDate;

    @Column(name = "LAST_UPDATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date lastUpdatedDate;

    @Column(name = "STATUS")
    private int status;

}