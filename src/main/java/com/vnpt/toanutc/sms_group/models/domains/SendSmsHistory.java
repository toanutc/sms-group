package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import lombok.Builder;
import lombok.Data;

@Data
@Entity
@Table(name = "send_sms_history")
@Builder
public class SendSmsHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2261936136903608602L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "FROM_PHONE_NUMBER")
    private String fromPhoneNumber;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "MESSAGE_PRICE")
    private int messagePrice;

    @Column(name = "TO_PHONE_NUMBER")
    private String toPhoneNumber;

    @Column(name = "SENT_TIME")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date sentTime;

    @Column(name = "RECEIVED_TIME")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date receivedTime;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "CREATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

}