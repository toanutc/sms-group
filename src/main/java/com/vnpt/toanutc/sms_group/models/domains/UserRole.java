package com.vnpt.toanutc.sms_group.models.domains;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_role")
public class UserRole implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4117942183537420305L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "ROLE_CODE")
    private String roleCode;

    @Column(name = "DESCRIPTION")
    private String description;
}