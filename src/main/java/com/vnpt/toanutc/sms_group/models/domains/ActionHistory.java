package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "action_history")
public class ActionHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8835967132169800442L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "ACTION_CODE")
    private String actionCode;

    @Column(name = "PARAMS")
    private String params;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "CHANNEL")
    private String channel;

    @Column(name = "IP")
    private String ip;
    
    @Column(name = "CREATED_BY")
    private String createdBy;
    
    @Column(name = "CREATED_DATE")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

}