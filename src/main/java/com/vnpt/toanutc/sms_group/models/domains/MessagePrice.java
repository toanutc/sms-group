package com.vnpt.toanutc.sms_group.models.domains;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "message_prices")
public class MessagePrice implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = -8147429060089768200L;

    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "SUB_MAX_VALUE")
    private int subMaxValue;

    @Column(name = "MESSAGE_PRICE")
    private int messagePrice;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "TYPE")
    private int type;

    @Column(name = "CREATED_DATE")
    private Timestamp createdDate;

}