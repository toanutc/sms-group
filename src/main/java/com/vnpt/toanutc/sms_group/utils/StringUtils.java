package com.vnpt.toanutc.sms_group.utils;

public class StringUtils {

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.isEmpty();
    }
}
